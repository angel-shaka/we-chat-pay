﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Songzeyu.Helper;
using Songzeyu.Pay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Songzeyu.Extensions;
using Songzeyu.Helper.Extensions;

namespace Angelshaka.Controllers.WxPay
{
    /// <summary>
    /// 资金账单
    /// </summary>
    [Route("wxpay/[controller]")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "微信支付")]
    public class FundflowbillController : ControllerBase
    {
        /// <summary>
        /// 申请资金账单
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<object> Get(string bill_date)
        {
            //bill_date = "2021-12-31";
            string response = await WeChatPay.Context.Fundflowbill(bill_date);

            if (response.IsNull()) return new { code = 0, msg = "该日期没有账单" };

            var res = response.DeJson<WxResponse>();

            var fileBytes = await WeChatPay.Context.Billdownload(res.download_url);

            if (fileBytes.ComputeSHA1() != res.hash_value) return new { code = 0, msg = "数字指纹不一致" };

            return new { code = 1, data = fileBytes.ToUTF8String(), };

        }
    }
}
