﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Songzeyu.Helper;
using Songzeyu.Pay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angelshaka.Controllers
{
    /// <summary>
    /// 微信支付_订单查询
    /// </summary>
    [Route("wxpay/[controller]")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "微信支付")]
    public class TransactionController : ControllerBase
    {
        /// <summary>
        /// 查询订单
        /// </summary>
        /// <param name="out_trade_no">商户订单号</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<object> Get(string out_trade_no)
        {
            string result = await WeChatPay.Context.Transaction(out_trade_no);

            return new { code = 1, data = result };
        }

    }
}
