﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Songzeyu.Helper;
using Songzeyu.Pay;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Songzeyu.Extensions;
using Songzeyu.Helper.Extensions;

namespace Angelshaka.Controllers
{
    /// <summary>
    /// 微信支付_native
    /// </summary>
    [Route("wxpay/[controller]")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "微信支付")]
    public class NativeController : ControllerBase
    {
        private static object locking = new object();
        /// <summary>
        /// 下单生成支付链接
        /// </summary>
        /// <param name="money">金额_单位_分</param>
        /// <param name="desc">描述</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<object> Get(ulong money, string desc)
        {

            string out_trade_no;
            lock (locking) out_trade_no = "Angelshakawx_" + DateHelper.GetUtcTicks();

            string result = await WeChatPay.Context.Native(money, out_trade_no, desc, "https://angelshaka.vip:4000/wxpay/Notify");

            return new { code = 1, data = new { out_trade_no = out_trade_no, code_url = result.DeJson<WxResponse>().code_url } };

        }




    }
}
