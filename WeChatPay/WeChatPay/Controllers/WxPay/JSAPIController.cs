﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Songzeyu.Extensions;
using Songzeyu.Helper;
using Songzeyu.Helper.Extensions;
using Songzeyu.Pay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angelshaka.Controllers.WxPay
{
    /// <summary>
    /// 微信支付_JSAPI
    /// </summary>
    [Route("wxpay/[controller]")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "微信支付")]
    public class JSAPIController : ControllerBase
    {

        /// <summary>
        /// JSAPI下单
        /// </summary>
        /// <param name="code">公众号_用户授权code</param>
        /// <param name="money">金额_分</param>
        /// <param name="desc">商品描述</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<object> HttpPost(string code, ulong money, string desc)
        {

            var out_trade_no = ToolHelper.NewGuid();
            string result = await WeChatPay.Context.JSAPI(code, money, out_trade_no, desc, "https://angelshaka.vip:4000/wxpay/Notify");

            var model = result.DeJson<ResponseModel>();

            var timeStamp = DateHelper.GetTimeStamp();
            var nonceStr = ToolHelper.NewGuid();
            var package = $"prepay_id={model.prepay_id}";
            var paySign = WeChatPay.Context.GetPaySign(timeStamp, nonceStr, package);

            var r = new
            {
                appId = WeChatPay.Context.wx_appid,     //公众号ID，由商户传入     
                timeStamp = timeStamp,     //时间戳，自1970年以来的秒数     
                nonceStr = nonceStr,      //随机串     
                package = package,
                signType = "RSA",     //微信签名方式：     
                paySign = paySign//微信签名 
            };

            return new { code = 1, data = r };

        }



    }



    /// <summary>
    /// 微信响应
    /// </summary>
    public class ResponseModel
    {
        public string prepay_id;
    }


    //微信js方法样例：
    //    function onBridgeReady()
    //    {
    //        WeixinJSBridge.invoke('getBrandWCPayRequest', {
    //            "appId": "wx2421b1c4370ec43b",     //公众号ID，由商户传入     
    //        "timeStamp": "1395712654",     //时间戳，自1970年以来的秒数     
    //        "nonceStr": "e61463f8efa94090b1f366cccfbbb444",      //随机串     
    //        "package": "prepay_id=up_wx21201855730335ac86f8c43d1889123400",
    //        "signType": "RSA",     //微信签名方式：     
    //        "paySign": "oR9d8PuhnIc+YZ8cBHFCwfgpaK9gd7vaRvkYD7rthRAZ\/X+QBhcCYL21N7cHCTUxbQ+EAt6Uy+lwSN22f5YZvI45MLko8Pfso0jm46v5hqcVwrk6uddkGuT+Cdvu4WBqDzaDjnNa5UK3GfE1Wfl2gHxIIY5lLdUgWFts17D4WuolLLkiFZV+JSHMvH7eaLdT9N5GBovBwu5yYKUR7skR8Fu+LozcSqQixnlEZUfyE55feLOQTUYzLmR9pNtPbPsu6WVhbNHMS3Ss2+AehHvz+n64GDmXxbX++IOBvm2olHu3PsOUGRwhudhVf7UcGcunXt8cqNjKNqZLhLw4jq\/xDg==" //微信签名 
    //    },
    //    function(res) {
    //            if (res.err_msg == "get_brand_wcpay_request:ok")
    //            {
    //                // 使用以上方式判断前端返回,微信团队郑重提示：
    //                //res.err_msg将在用户支付成功后返回ok，但并不保证它绝对可靠。
    //            }
    //        });
    //    }
    //    if (typeof WeixinJSBridge == "undefined") {
    //        if (document.addEventListener) {
    //            document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
    //        } else if (document.attachEvent)
    //    {
    //        document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
    //        document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
    //    }
    //    } else
    //    {
    //        onBridgeReady();
    //    }
}
