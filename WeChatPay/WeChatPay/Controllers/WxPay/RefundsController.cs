﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Songzeyu.API;
using Songzeyu.Helper;
using Songzeyu.Pay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angelshaka.Controllers.WxPay
{
    /// <summary>
    /// 退款
    /// </summary>
    [Route("wxpay/[controller]")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "微信支付")]
    public class RefundsController : ControllerBase
    {
        /// <summary>
        /// 申请退款
        /// </summary>
        /// <param name="out_trade_no">商户订单号</param>
        /// <param name="money">金额_单位_分</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<object> Post(string out_trade_no, ulong money)
        {
            string result = await WeChatPay.Context.Refunds(out_trade_no, money);
            return new { code = 1, data = result };
        }








    }
}
