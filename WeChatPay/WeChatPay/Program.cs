using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Songzeyu.Helper;
using Songzeyu.Pay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angelshaka_wxpay
{
    public class Program
    {
        public static void Main(string[] args)
        {
            NLogHelper.Info("程序启动");

            //配置微信支付
            var appid = ""; //微信生成的appid
            var mchid = ""; //商户号
            var serial_no = ""; //微信支付证书序列号
            var apiclient_keyFile = $"{System.AppDomain.CurrentDomain.BaseDirectory}Licence/apiclient_key.pem"; //证书密钥_文件地址_[如果有apiclient_key参数，则本参数可以为空]
            var AES_KEY = ""; //[非必填_回调报文验签解密使用]_APIv3密钥_微信_商户平台_API安全_获取该密钥
            var appsecret = ""; // [非必填_JSAPI使用]_公众号的appsecret
            var notify_url = "https://angelshaka.vip:4000/wxpay/Notify"; //[非必填]回调地址
            WeChatPay.Init(new WeChatPay(appid, mchid, serial_no, apiclient_keyFile, AES_KEY, appsecret, notify_url));


            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    //webBuilder.UseUrls("https://*:443;http://*:80");
                    webBuilder.UseUrls("http://*:9000");
                    webBuilder.UseStartup<Startup>();
                }).UseWindowsService();
    }
}
