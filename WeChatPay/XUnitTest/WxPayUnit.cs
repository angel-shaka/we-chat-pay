using Songzeyu.Extensions;
using Songzeyu.Helper;
using Songzeyu.Helper.Extensions;
using Songzeyu.Pay;
using System;
using System.Net;
using Xunit;

namespace XUnitTest
{
    public class WxPayUnit
    {
        static WxPayUnit()
        {
            //配置微信支付
            var appid = ""; //微信生成的appid
            var mchid = ""; //商户号
            var serial_no = ""; //微信支付证书序列号
            var apiclient_keyFile = $"{System.AppDomain.CurrentDomain.BaseDirectory}Licence/apiclient_key.pem"; //证书密钥_文件地址_[如果有apiclient_key参数，则本参数可以为空]
            var AES_KEY = ""; //[非必填_回调报文验签解密使用]_APIv3密钥_微信_商户平台_API安全_获取该密钥
            var appsecret = ""; // [非必填_JSAPI使用]_公众号的appsecret
            var notify_url = "https://angelshaka.vip:4000/wxpay/Notify"; //[非必填]回调地址
            WeChatPay.Init(new WeChatPay(appid, mchid, serial_no, apiclient_keyFile, AES_KEY, appsecret, notify_url));

        }

        [Theory]
        [InlineData(1,"一分钱")]
        [InlineData(3,"三分钱")]
        public async void NativeController(ulong money, string desc)
        {
            string out_trade_no = "Angelshakawx_" + DateHelper.GetUtcTicks();

            string result = await WeChatPay.Context.Native(money, out_trade_no, desc, "https://angelshaka.vip:4000/wxpay/Notify");

            Assert.NotEmpty(result);
            Assert.True(result.DeJson<WxResponse>().code_url.IndexOf("weixin://") > -1);
           
        }

        /// <summary>
        /// 订单查询
        /// </summary>
        [Fact]
        public async void Transaction()
        {
            string out_trade_no = "Angelshakawx_" + DateHelper.GetUtcTicks();

            await WeChatPay.Context.Native(1, out_trade_no, "1分钱", "https://angelshaka.vip:4000/wxpay/Notify");
            string result = await WeChatPay.Context.Transaction(out_trade_no);

            Assert.NotEmpty(result);
            Assert.True(result.IndexOf("trade_state_desc") > -1);

        }
        
    }
}
